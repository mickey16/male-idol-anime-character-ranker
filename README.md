https://mickey16.gitlab.io/male-idol-anime-character-ranker

Character preference ranker for male idol animes.

Currently includes:
<ul>
    <li>  B-Project
    <li>  Dream Festival
    <li>  High School Star Musical
    <li>  IDOLiSH7
    <li>  King of Prism
    <li>  Marginal #4
    <li>  Uta no Prince-sama
    <li>  Custom
</ul>